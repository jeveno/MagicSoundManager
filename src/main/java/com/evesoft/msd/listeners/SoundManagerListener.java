/**
 * 
 */
package com.evesoft.msd.listeners;

import java.util.EventListener;

import com.evesoft.msd.Events.SoundManagerEvent;

/**
 * @author Jérôme
 *
 */
public interface SoundManagerListener extends EventListener {

    /**
     * @param evt
     */
    public void update(SoundManagerEvent evt);
}
