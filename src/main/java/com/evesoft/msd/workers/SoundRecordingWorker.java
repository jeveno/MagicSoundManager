package com.evesoft.msd.workers;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
import javax.swing.JPanel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * SoundDetectionTask
 * @author jerome
 * 
 * SwingWorker
 * Parameters:
 * <T> the result type returned by this SwingWorker's doInBackground and get methods
 * <V> the type used for carrying out intermediate results by this SwingWorker's publish and process methods
 */
public class SoundRecordingWorker extends ASoundWorker {

    private static final int AUDIO_FRAMES = 8192;
    private ByteArrayOutputStream recordBytes;
    private TargetDataLine audioLine;
    private AudioFormat format;

    private float sampleRate;;
    private int sampleSizeInBits;
    private int channels;
    private boolean signed = true;
    private boolean bigEndian = true;

    private boolean isRunning;

    private JPanel panelDetection;

    private Logger logger = LogManager.getLogger(this.getClass());


    @SuppressWarnings("unused")
    private SoundRecordingWorker() {

    }

    @Override
    protected void process(List<Boolean> chunks) {

	for(boolean result:chunks) {

	    if (result) {		
		panelDetection.setBackground(Color.RED);
	    } else {
		panelDetection.setBackground(Color.GREEN);
	    }
	}
    }

    @Override
    protected void done() {
	logger.info("done");

	// TODO Auto-generated method stub
	super.done();
    }


    /**
     * SoundDetectionTask
     * @param sampleRate
     * @param sampleSizeInBits
     * @param channels
     * @param frequencyToDetect
     * @param panelDetection
     */
    public SoundRecordingWorker(float sampleRate, int sampleSizeInBits, int channels, JPanel panelDetection) {
	this.sampleRate = sampleRate;
	this.sampleSizeInBits = sampleSizeInBits;
	this.channels = channels;
	this.panelDetection = panelDetection;
    }


    /**
     * isRunning
     * @return boolean
     */
    public boolean isRunning() {
	return isRunning;
    }

    /**
     * Stop recording sound
     */
    public void stop() {

	try {

	    logger.info("[SoundRecordingWorker][stop]");

	    isRunning = false;

	    if (audioLine != null) {
		logger.info("drain");

		audioLine.stop();
		audioLine.drain();

		logger.info("close");

		audioLine.close();
	    }

	    this.cancel(true);
	    
	} catch(Exception ex) {
	    logger.error("[SoundRecordingWorker][stop] : Erreur à l'arret de l'enregistrement : {}" + ex.getMessage());
	}
    }

    /**
     * Defines a default audio format used to record
     * @return AudioFormat
     */
    AudioFormat getAudioFormat() {
	return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
    }


    @Override
    protected Boolean doInBackground() throws Exception {

	Boolean result = new Boolean(false);

	logger.info("[SoundRecordingWorker][doInBackground] : Run ");

	try{
	    format = getAudioFormat();
	    DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);

	    // checks if system supports the data line
	    if (!AudioSystem.isLineSupported(info)) {
		throw new LineUnavailableException("The system does not support the specified format.");
	    }

	    audioLine = AudioSystem.getTargetDataLine(format);

	    audioLine.addLineListener(this);
	    audioLine.open(format);
	    audioLine.start();

	    byte[] buffer = new byte[AUDIO_FRAMES];

	    int bytesRead = 0;

	    recordBytes = new ByteArrayOutputStream();
	    isRunning = true;

	    while (isRunning) {
		//read the datas
		bytesRead = audioLine.read(buffer, 0, buffer.length);
		recordBytes.write(buffer, 0, bytesRead);
	    }

	    result = true;

	} catch (Exception ex) {
	    logger.error("[SoundRecordingWorker][doInBackground] : Erreur lors de l'enregistrement {} ", ex.getMessage());

	} finally {
	    logger.info("[SoundRecordingWorker][doInBackground] : End ");
	    
	    if (audioLine != null && audioLine.isOpen()) {
		audioLine.stop();
	    }
	}

	return result;
    }


    /**
     * Save recorded sound data into a .wav file format.
     * @param wavFile The file to be saved.
     * @throws IOException if any I/O error occurs.
     */
    public void save(File wavFile) throws IOException {

	logger.info("[SoundRecordingManager][update] :Debut save:" + wavFile.getAbsolutePath());

	byte[] audioData = recordBytes.toByteArray();
	ByteArrayInputStream bais = new ByteArrayInputStream(audioData);
	AudioInputStream audioInputStream = new AudioInputStream(bais, format,
		audioData.length / format.getFrameSize());

	AudioSystem.write(audioInputStream, AudioFileFormat.Type.WAVE, wavFile);

	audioInputStream.close();
	recordBytes.close();

	logger.info("Fin save:");
    }

    /* (non-Javadoc)
     * @see com.evesoft.msd.workers.ASoundWorker#update(javax.sound.sampled.LineEvent)
     */
    @Override
    public void update(LineEvent le) {
	LineEvent.Type type = le.getType();
	
	logger.debug("[SoundRecordingManager][update] : type={}", type.toString());
	
	if (type == LineEvent.Type.OPEN) {

	} else if (type == LineEvent.Type.CLOSE) {

	} else if (type == LineEvent.Type.START) {

	} else if (type == LineEvent.Type.STOP) {

	}
    }
}
