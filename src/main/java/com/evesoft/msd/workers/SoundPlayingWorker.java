package com.evesoft.msd.workers;

import java.io.File;
import java.util.List;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * SoundDetectionTask
 * @author JEVENO
 * 
 * SwingWorker
 * Parameters:
 * <T> the result type returned by this SwingWorker's doInBackground and get methods
 * <V> the type used for carrying out intermediate results by this SwingWorker's publish and process methods
 */
public class SoundPlayingWorker extends ASoundWorker {

    private boolean isRunning;
    private Clip clip;
    private AudioInputStream audioIn;
    private String fileName;
    private int numberOfLoops;
    private Logger logger = LogManager.getLogger(this.getClass());

    private String oldValue, newValue;
    
    
    @Override
    protected void process(List<Boolean> chunks) {
	logger.info("process - unused");
    }

    @Override
    protected void done() {
	logger.info("Done");
	// TODO Auto-generated method stub
	super.done();
    }

    /**
     * 
     */
    @SuppressWarnings("unused")
    private SoundPlayingWorker() {

    }

    /**
     * @param fileName
     * @param numberOfLoops 
     * @throws Exception 
     */
    public SoundPlayingWorker(String fileName, int numberOfLoops) throws Exception {

	try {  
	    logger.info("Constructeur SoundPlayingWorker : " + fileName);
	    this.fileName = fileName;
	    this.numberOfLoops = numberOfLoops;
	    // Get a sound clip resource.
	    clip = AudioSystem.getClip();
	    
	} catch (Exception e) {
	    logger.error("Erreur constructeur SoundPlayingWorker : " + e.getMessage(), e);
	    throw e;
	} 
    }


    /**
     * isRunning
     * @return
     */
    public boolean isRunning() {
	return isRunning;
    }

    /**
     * Stop playing sound
     */
    public void stop() {
	try {
	    logger.info("[SoundPlayingWorker][stop]");
	    clip.stop();
	    isRunning = false;    
	    this.cancel(true);
	} catch (Exception ex) {
	    logger.error("[SoundPlayingWorker][stop] error={}", ex.getMessage());
	}
    }


    @Override
    protected Boolean doInBackground() throws Exception {

	try {

	    logger.info("[SoundPlayingWorker][doInBackground] : Begin");

	    isRunning = true;

	    File file = new File(fileName);
	    
	    if (!file.exists()) {	
		logger.error("[SoundPlayingWorker][doInBackground] - Le fichier {} n'existe pas", fileName);
		throw new Exception("Le fichier n'existe pas");
	    } else {
		logger.info("[SoundPlayingWorker][doInBackground] - Le fichier {} existe", fileName);
	    }

	    audioIn = AudioSystem.getAudioInputStream(this.getClass().getClassLoader().getResourceAsStream(fileName));

	    clip.addLineListener(this);

	    // Open audio clip and load samples from the audio input stream.
	    clip.open(audioIn);
	    clip.start();
	    clip.loop(numberOfLoops);

	} catch (Exception e) {
	    logger.error("[SoundPlayingWorker][doInBackground] : ERROR={}", e.getMessage());
	} finally {

	    logger.info("[SoundPlayingWorker][doInBackground] : End");
	}

	return true;
    }

    /* (non-Javadoc)
     * @see com.evesoft.msd.workers.ASoundWorker#update(javax.sound.sampled.LineEvent)
     */
    @Override
    public void update(LineEvent le) {
	LineEvent.Type type = le.getType();

	logger.debug("[SoundPlayingWorker][update] : LineEvent.Type={}", type.toString());

	oldValue = newValue;
	
	if (type == LineEvent.Type.OPEN) {
	    newValue = "OPEN";
	} else if (type == LineEvent.Type.CLOSE) {
	    newValue = "CLOSE";
	} else if (type == LineEvent.Type.START) {
	    newValue = "START";
	} else if (type == LineEvent.Type.STOP) {
	    //Reading is over
	    logger.debug("[SoundPlayingWorker][update]  : reading of file is over");
	    newValue = "STOP";
	    clip.close();
	}
	
	this.firePropertyChange("readStatus", oldValue, newValue);
    }
}
