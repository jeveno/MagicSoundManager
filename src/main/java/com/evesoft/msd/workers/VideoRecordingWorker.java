package com.evesoft.msd.workers;

import java.util.List;

import javax.swing.SwingWorker;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * SoundDetectionTask
 * @author JEVENO
 * 
 * SwingWorker
 * Parameters:
 * <T> the result type returned by this SwingWorker's doInBackground and get methods
 * <V> the type used for carrying out intermediate results by this SwingWorker's publish and process methods
 */
public class VideoRecordingWorker extends SwingWorker<Boolean, Boolean> {

    private boolean isRunning;

    private Logger logger = LogManager.getLogger(this.getClass());


    @Override
    protected void process(List<Boolean> chunks) {
	logger.info("process - unused");
    }

    @Override
    protected void done() {
	logger.info("Done");
	// TODO Auto-generated method stub
	super.done();
    }

//    /**
//     * 
//     */
//    @SuppressWarnings("unused")
//    private VideoRecordingWorker() {
//
//    }

    /**
     * @param fileName
     * @param numberOfLoops 
     * @throws Exception 
     */
    public VideoRecordingWorker() throws Exception {

	try {  
	  
	} catch (Exception e) {
	    logger.error("Erreur constructeur VideoRecordingWorker : " + e.getMessage(), e);
	    throw e;
	} 
    }


    /**
     * isRunning
     * @return
     */
    public boolean isRunning() {
	return isRunning;
    }

    /**
     * Stop playing sound
     */
    public void stop() {
	try {
	    logger.info("[VideoRecordingWorker][stop]");
	   
	    isRunning = false;    
	    this.cancel(true);
	} catch (Exception ex) {
	    logger.error("[VideoRecordingWorker][stop] error={}", ex.getMessage());
	}
    }


    @Override
    protected Boolean doInBackground() throws Exception {

	try {

	    logger.info("[VideoRecordingWorker][doInBackground] : Begin");


	} catch (Exception e) {
	    logger.error("[VideoRecordingWorker][doInBackground] : ERROR={}", e.getMessage());
	} finally {

	    logger.info("[VideoRecordingWorker][doInBackground] : End");
	}

	return true;
    }

}
