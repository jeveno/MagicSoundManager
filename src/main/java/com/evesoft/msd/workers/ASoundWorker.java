/**
 * 
 */
package com.evesoft.msd.workers;

import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.swing.SwingWorker;

/**
 * @author Jérôme
 *
 */
public abstract class ASoundWorker extends SwingWorker<Boolean, Boolean> implements LineListener  {


    public abstract void update(LineEvent le);
    
    
}
