package com.evesoft.msd.workers;

import java.util.List;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.evesoft.msd.helper.Utils;


/**
 * SoundDetectionTask
 * @author JEROME E
 * 
 * SwingWorker
 * Parameters:
 * <T> the result type returned by this SwingWorker's doInBackground and get methods
 * <V> the type used for carrying out intermediate results by this SwingWorker's publish and process methods
 */
public class SoundDetectionWorker extends ASoundWorker {

    private static final int AUDIO_FRAMES = 8192;
    private TargetDataLine audioLine;
    private AudioFormat format;

    private float sampleRate;;
    private int sampleSizeInBits;
    private int channels;
    private boolean signed = true;
    private boolean bigEndian = true;
    private int frequencyToDetect;

    private boolean isRunning;

    private boolean oldValue;
    private boolean newValue;


    private Logger logger = LogManager.getLogger(this.getClass());

    /**
     * 
     */
    @SuppressWarnings("unused")
    private SoundDetectionWorker() {

    }

    @Override
    protected void process(List<Boolean> chunks) {

	if (isRunning) {
	    if (chunks != null) {
		for(boolean result:chunks) {

		    oldValue = newValue;
		    newValue = result;

		    //logger.debug("process chunks.size={}, oldValue={}, newValue={}", String.valueOf(chunks.size()), oldValue, newValue);

		    this.firePropertyChange("detection", oldValue, newValue);

		}
	    }

	}
    }

    @Override
    protected void done() {

	logger.info("Done");

	// TODO Auto-generated method stub
	super.done();
    }

    /**
     * SoundDetectionTask
     * @param sampleRate
     * @param sampleSizeInBits
     * @param channels
     * @param frequencyToDetect 
     */
    public SoundDetectionWorker(float sampleRate, int sampleSizeInBits, int channels, int frequencyToDetect) {
	this.sampleRate = sampleRate;
	this.sampleSizeInBits = sampleSizeInBits;
	this.channels = channels;
	this.frequencyToDetect =  frequencyToDetect;
    }


    /**
     * isRunning
     * @return
     */
    public boolean isRunning() {
	return isRunning;
    }

    /**
     * Stop detecting sound
     */
    public void stop() {

	try {
	    logger.info("[SoundDetectingWorker][stop]");

	    isRunning = false;

	    if (audioLine != null) {

		logger.info("drain");
		audioLine.stop();
		audioLine.drain();

		logger.info("close");
		audioLine.close();
	    }

	    this.cancel(true);

	} catch (Exception ex) {
	    logger.error("[SoundDetectingWorker][stop] error={}", ex.getMessage());
	}
    }

    /**
     * Defines a default audio format used to record
     * @return AudioFormat
     */
    private AudioFormat getAudioFormat() {
	return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
    }


    /**
     * findFrequency
     * @param frequencyToDetect 
     * @param doubleData
     * @param sampleRate
     * @param audioFrames
     * @return Boolean
     */
    public Boolean detectFrequency(int frequencyToDetect, double doubleData[], float sampleRate, int audioFrames){  

	Boolean result = new Boolean(false);

	FastFourierTransformer transformer = new FastFourierTransformer(DftNormalization.STANDARD);  

	double frequency;  
	Complex[] cmplx= transformer.transform(doubleData, TransformType.FORWARD);  
	double real;  
	double im;  
	double mag[] = new double[cmplx.length];  

	for(int i = 0; i < cmplx.length; i++){  
	    real = cmplx[i].getReal();  
	    im = cmplx[i].getImaginary();
	    mag[i] = 20 * Math.log10(Math.sqrt((real * real) + (im*im)));  
	}  

	double peak1 = -1, peak2 = -1, peak3 =-1;
	int index=-1;

	//frequencyToDetect
	//6766.809	-48.287216186
	//9022.412	-72.078811645
	//11278.015	-65.371330261

	//We search a signal between 6750 and 6800 Hz
	int indexFreqMin = (int) (((6766-40) * audioFrames) / sampleRate);
	int indexFreqMax = (int) (((6766 + 40) * audioFrames) / sampleRate);

	//Calcul du pic

	//for(int i = 0; i < cmplx.length; i++){
	for(int i = indexFreqMin; i < indexFreqMax; i++){
	    if(peak1 < mag[i]){  
		index=i;  
		peak1= mag[i];  
	    }  
	}  


	//Si le pic est suffisamment important, on consid�re que le signal est detecte
	if (peak1 > 70) {
	    result = true;
	    frequency = -1;
	} else {
	    //frequency = (sampleRate * (index)) / (audioFrames);  
	    frequency = -1;
	}

	System.out.println(String.format("peak1=%1s peak3=%3s", peak1, peak3));
	
	//frequency = (sampleRate * (index)) / (audioFrames);  
	//System.out.println("Index: "+index+", Frequency: "+frequency+ ", peak=" + peak);  

	return result; 
    }

    @Override
    protected Boolean doInBackground() throws Exception {

	logger.info("[SoundDetectionWorker][doInBackground] : Run ");

	try{

	    format = getAudioFormat();
	    DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);

	    // checks if system supports the data line
	    if (!AudioSystem.isLineSupported(info)) {
		throw new LineUnavailableException("The system does not support the specified format.");
	    }

	    audioLine = AudioSystem.getTargetDataLine(format);
	    audioLine.addLineListener(this);
	    audioLine.open(format);
	    audioLine.start();

	    byte[] buffer = new byte[AUDIO_FRAMES];
	    double doubleData[] = new double[AUDIO_FRAMES/2];

	    isRunning = true;

	    while (isRunning) {
		//read the datas
		audioLine.read(buffer, 0, buffer.length);

		//double conversion
		Utils.byteToDouble(buffer, doubleData);

		//Retrieve the frequency from the byte datas
		boolean detection = detectFrequency(frequencyToDetect, doubleData, format.getSampleRate(), AUDIO_FRAMES/2);

		//publish the result of detection (call the method process which is thread-safe)
		publish(detection);
	    }

	} catch (LineUnavailableException e) {
	    logger.error("[SoundDetectionWorker][doInBackground] : {}", e.getMessage());
	} finally {
	    logger.info("[SoundDetectionWorker][doInBackground] : End ");

	    if (audioLine != null && audioLine.isOpen()) {
		audioLine.stop();
	    }
	}

	return false;
    }

    /* (non-Javadoc)
     * @see com.evesoft.msd.workers.ASoundWorker#update(javax.sound.sampled.LineEvent)
     */
    @Override
    public void update(LineEvent le) {
	LineEvent.Type type = le.getType();

	//logger.debug("[SoundDetectionWorker][update] : type={}", type.toString());

	if (type == LineEvent.Type.OPEN) {

	} else if (type == LineEvent.Type.CLOSE) {

	} else if (type == LineEvent.Type.START) {

	} else if (type == LineEvent.Type.STOP) {

	}
    }



}

