package com.evesoft.msd.business;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JPanel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.evesoft.msd.Events.SoundManagerEvent;
import com.evesoft.msd.listeners.SoundManagerListener;
import com.evesoft.msd.workers.SoundDetectionWorker;
import com.evesoft.msd.workers.SoundPlayingWorker;
import com.evesoft.msd.workers.SoundRecordingWorker;

/**
 * 
 * @author Jerôme
 *
 */
public class SoundManager implements PropertyChangeListener {

    private static SoundManager instance;
 
    private SoundRecordingWorker recordingWorker;
    private SoundDetectionWorker detectionWorker;
    private SoundPlayingWorker playingWorker;

    //TODO
    private int frequencyToDetect = 6775;
    private float sampleRate = 44100;
    private int sampleSizeInBits = 16;
    private int channels = 1;
    private TimerTask task;
    private int nbAlertes;
    private Timer timer;
    private long chrono = 0;
    private long beginChrono;

    private Logger logger = LogManager.getLogger(this.getClass());


    private JPanel panelDetection;

    Map<String, SoundManagerListener> listenerMap = null;


    public enum ESoundManagerState {	
	WAITING,
	RECORDING,
	DETECTING	
    }

    public enum EAlertState {	
	NONE,
	WAITING_FOR_ALERT,
	ALERTING,
	WAITING_FOR_STOPPING_ALERT
    }

    private ESoundManagerState soundManagerState;

    private EAlertState alertState;


    /**
     * @param listener
     */
    public void addSoundManagerListener(SoundManagerListener listener) {

	if(listenerMap == null ){
	    listenerMap = new HashMap<String, SoundManagerListener>();
	}

	listenerMap.put(null, listener);
    }


    private void fireEvent(String msg) {

	if (listenerMap != null) {
	    for (SoundManagerListener listener : listenerMap.values()) {

		SoundManagerEvent evt = new SoundManagerEvent(msg);
		listener.update(evt);
	    }
	}
    }

    /**
     * @param panelDetection
     */
    public void setPanelDetection(JPanel panelDetection) {
	this.panelDetection = panelDetection;
    }


    /**
     * Private constructor because singleton
     */
    private SoundManager() {

    }

    /**
     * Singleton
     * @return SoundManager
     */
    public static SoundManager getInstance(){
	if(instance==null) {
	    instance = new SoundManager();
	    instance.soundManagerState = ESoundManagerState.WAITING;
	    instance.alertState = EAlertState.NONE;
	}
	return instance;
    }

    /**
     * stopRecording
     * @return boolean
     */
    public boolean stopRecording() {

	if (soundManagerState == ESoundManagerState.RECORDING) {

	    logger.info("[SoundManager][stopRecording] - Begin");

	    boolean result = false;

	    try {
		recordingWorker.stop();

		String executionPath = System.getProperty("user.dir");
		
		
		File audioFile = new File(executionPath + "/records/record.wav");

		logger.info("path=" + audioFile.getAbsolutePath());

		try {
		    String routePath = this.getClass().getClassLoader().getResource(File.separator).getPath();

		    logger.info("routePath=" + routePath);
		} catch (Exception ex) {
		    logger.error("Erreur lors de la recuperation de routePath : " + ex.getMessage(), ex);
		}

		if (audioFile.exists()){
		    logger.info("le fichier existe, on le supprime");

		    if(!audioFile.delete()) {	    
			logger.info("erreur lors de la suppression du fichier");
		    }
		}

		logger.info("avant save");

		recordingWorker.save(audioFile);

		logger.info("apres save");

		result = true;
	    } catch (Exception ex) {
		logger.error("Erreur dans stopRecording : " + ex.getMessage(), ex);
	    } finally {
		this.soundManagerState = ESoundManagerState.WAITING;
		logger.info("[SoundManager][stopRecording] - End");
	    }

	    return result;

	} else {
	    return false;  
	} 

    }

    /**
     * stopDetecting
     */
    public void stopDetecting() {

	if (soundManagerState == ESoundManagerState.DETECTING) {

	    logger.info("[SoundManager][stopDetecting] : chrono=" + String.valueOf(chrono));
	    
	    detectionWorker.stop();	
	    stopTimer();
	    if (alertState == EAlertState.ALERTING) {
		stopPlaying();
	    }
	    soundManagerState = ESoundManagerState.WAITING;
	}
    }

    /**
     * stopPlaying
     */
    public void stopPlaying() {

	logger.info("[SoundManager][stopPlaying]");
	playingWorker.stop();		
    }

    /**
     * detectSound
     * @param listener listener
     */
    public void detectSound() {

	logger.info("[SoundManager][detectSound]");

	if (soundManagerState == ESoundManagerState.WAITING) {

	    this.soundManagerState = ESoundManagerState.DETECTING;

	    detectionWorker = new SoundDetectionWorker(sampleRate, sampleSizeInBits, channels, frequencyToDetect);
	    detectionWorker.addPropertyChangeListener(this);
	    detectionWorker.execute();

	}
    }


    /**
     * recordAudio
     * @param listener listener
     */
    public void recordAudio() {

	logger.info("[SoundManager][recordAudio]");

	if (soundManagerState == ESoundManagerState.WAITING) {

	    this.soundManagerState = ESoundManagerState.RECORDING;

	    recordingWorker = new SoundRecordingWorker(sampleRate, sampleSizeInBits, channels, panelDetection);
	    recordingWorker.addPropertyChangeListener(this);
	    recordingWorker.execute();
	}
    }


    /**
     * playSound
     * @param fileName
     * @param numberOfLoops
     * @return 
     */
    public boolean playSound(String fileName, int numberOfLoops) {

	logger.info("[SoundManager][playSound]");

	try {
	    playingWorker = new SoundPlayingWorker(fileName, numberOfLoops);
	    
	} catch (Exception e) {
	    try {
		logger.error("[SoundManager][playSound] : first error");
		playingWorker = new SoundPlayingWorker(fileName, numberOfLoops);
	    } catch (Exception e1) {
		logger.error("[SoundManager][playSound] : second error");
		return false;
	    }
	}
	
	
	playingWorker.addPropertyChangeListener(this);
	playingWorker.execute();
	
	return true;
    }


    /* (non-Javadoc)
     * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
	// TODO Auto-generated method stub

	//logger.debug("[SoundManager][propertyChange] : evt={}", evt.toString());

	switch (evt.getPropertyName()) {

	case "detection":

	    boolean detectionResult = (boolean) evt.getNewValue();					

	    logger.info("[SoundManager][propertyChange] : Resultat detection={}, alertState={}", String.valueOf(detectionResult), alertState.toString());

	    //Si detection non enclenchee, on attend 1 seconde avant de l'enclencher
	    if(alertState == EAlertState.NONE && detectionResult) {

		logger.info("[SoundManager][propertyChange] : on prepare une alerte. Creation Timer");

		task = new TimerTask() {

		    @Override
		    public void run() {
			// TODO Auto-generated method stub

			//On active l'alarme
			beginChrono =  java.lang.System.currentTimeMillis(); 
			nbAlertes++;
			logger.info("[SoundManager][propertyChange] : Debut alerte numero {}", String.valueOf(nbAlertes));

			alertState = EAlertState.ALERTING;

			fireEvent("ALERT_ON");
		    }
		};

		timer = new Timer();
		timer.schedule(task,1000);
		alertState = EAlertState.WAITING_FOR_ALERT;

	    } else if (alertState == EAlertState.ALERTING && !detectionResult) {

		logger.info("[SoundManager][propertyChange] : plus de signal, on prepare la levee de l'alerte");
		
		//On ne detecte plus de signal, on attend 2 seconde sans rien avant de desactiver l'alerte
		if(task != null) task.cancel();

		task = new TimerTask() {

		    @Override
		    public void run() {
			//detection is now active
			alertState = EAlertState.NONE;

			long duration =  System.currentTimeMillis() - beginChrono;
			
			chrono = chrono + duration; 

			logger.info("[SoundManager][propertyChange] : fin alerte : duree alerte={}, chrono total={}",
				String.valueOf(duration),
				String.valueOf(chrono));

			beginChrono = 0;
			
			fireEvent("ALERT_OFF");
		    }
		};

		timer = new Timer();
		//the task will be executed in 2 seconds
		timer.schedule(task,2000);
		alertState = EAlertState.WAITING_FOR_STOPPING_ALERT;

	    } else if (alertState == EAlertState.WAITING_FOR_STOPPING_ALERT && detectionResult) {

		logger.info("[SoundManager][propertyChange] : detection signal donc on annule la levee de l'alerte");
		
		//Detection signal donc on annule la levee de l'alerte
		stopTimer();
		alertState = EAlertState.ALERTING;
		
	    } else if (alertState == EAlertState.WAITING_FOR_ALERT && !detectionResult) {

		long duration =  System.currentTimeMillis() - beginChrono;
		
		logger.info("[SoundManager][propertyChange] : plus de detection, on annule avant l'alerte. Duree signal={}", String.valueOf(duration));
		
		// plus de detection, on annule avant l'alerte
		stopTimer();
		alertState = EAlertState.NONE;
	    } 

	    if (detectionResult) {
		fireEvent("DETECTION_ACTIVE");
	    } else {
		fireEvent("DETECTION_INACTIVE");
	    }

	    break;
	    
	case "readStatus" :
	    
	    String value = (String) evt.getNewValue();
	    
	    if (value.equalsIgnoreCase("STOP")) {
		//on remonte l'info à l'ihm pour changer son etat
		fireEvent("READING_OVER");
	    }
	 	
	    break;
	    
	case "progress":

	    break;

	case "state":
	    

	    break;
	}
    }

    /**
     * stopTimer
     */
    private void stopTimer() {

	if(task != null) {
	    task.cancel();
	}

	if (timer != null) {
	    timer.cancel();
	}
    }

}
