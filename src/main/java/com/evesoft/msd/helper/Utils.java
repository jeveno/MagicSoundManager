/**
 * 
 */
package com.evesoft.msd.helper;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * @author Jérôme
 *
 */
public final class Utils {


    private Utils() {
	
    }
    
    /**
     * byteToDouble
     * @param byteData
     * @param doubleData
     */
    public static void byteToDouble(byte byteData[], double doubleData[]){  

	ByteBuffer buf= ByteBuffer.wrap(byteData);  
	buf.order(ByteOrder.BIG_ENDIAN);  
	int i=0;   

	while(buf.remaining()>2){  
	    short s = buf.getShort();  
	    doubleData[i] = (new Short(s)).doubleValue();  
	    ++i;  
	}  
    }  

    
}
