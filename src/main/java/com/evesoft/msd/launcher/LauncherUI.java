package com.evesoft.msd.launcher;

import java.awt.EventQueue;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.evesoft.msd.view.MainFrame;

/**
 * @author Jérôme
 *
 */
public class LauncherUI {

    private static Logger logger = LogManager.getLogger(new LauncherUI().getClass());
    
    /**
     * Launch the application.
     * @param args 
     */
    public static void main(String[] args) {
	
	EventQueue.invokeLater(new Runnable() {
	    
	    public void run() {
		try {
		    MainFrame window = new MainFrame();

		    try {
			UIManager.LookAndFeelInfo[] lafInfo = UIManager.getInstalledLookAndFeels();

//			for(LookAndFeelInfo info:lafInfo) {
//			    System.out.println(info.getClassName());
//			}

			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
			SwingUtilities.updateComponentTreeUI(window);

		    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
			    | UnsupportedLookAndFeelException e) {
			logger.error("[LauncherUI] - Erreur dans run", e);
		    }

		    window.setVisible(true);
		} catch (Exception e) {
		    logger.error("[LauncherUI] - Erreur generale dans run", e);
		}
	    }
	});
    }
}
