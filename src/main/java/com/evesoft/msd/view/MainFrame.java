package com.evesoft.msd.view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.evesoft.msd.Events.SoundManagerEvent;
import com.evesoft.msd.business.SoundManager;
import com.evesoft.msd.listeners.SoundManagerListener;

/**
 * @author Jérôme
 *
 */
public class MainFrame extends JFrame implements SoundManagerListener  {

    /**
     * 
     */
    private static final long serialVersionUID = -6262827748198860490L;

    private Logger logger = LogManager.getLogger(this.getClass());
    
    private boolean isRecording;
    private boolean isPlaying;
    private boolean isDetecting;
    private JButton btnRecord, btnDetect, btnPlay;


    private JPanel panelDetection = null;
    private JPanel panelAlert = null;

    private boolean isDetectionAlertActive;
    private boolean detectionResult;

    private TimerTask task;
    private int nbAlertes;
    private Timer timer;

    private long chrono = 0;

    private long beginChrono;

    private enum EDetectorState {
	STOPPED,
	DETECTING,
	WAITING_BEFORE_ALERT,
	ALERTING,
	WAITING_BEFORE_STOPPING_ALERT
    }

    private EDetectorState detectorState;


    /**
     * Create the application.
     */
    public MainFrame() {
	initialize();
	detectorState = EDetectorState.STOPPED;
    }

    private JLabel lblresultat;


    /**
     * @return JLabel
     */
    public JLabel getLblresultat() {
	if (lblresultat==null) {
	    lblresultat = new JLabel("");
	    lblresultat.setHorizontalAlignment(SwingConstants.CENTER);
	    lblresultat.setBounds(27, 166, 383, 20);
	}
	return lblresultat;
    }

    /**
     * @return JButton
     */
    public JButton getBtnRecord() {

	if (btnRecord == null) {
	    
	    logger.debug("Construction BtnRecord");
	    
	    btnRecord = new JButton("Record");
	    btnRecord.setLocation(165, 215);
	    btnRecord.setSize(116, 45);

	    btnRecord.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    getLblresultat().setText("");

		    logger.debug("[MainFrame][actionPerformed] : clic sur bouton record");

		    if (isRecording) {		
			SoundManager.getInstance().stopRecording();
			btnRecord.setText("Record");			
		    } else {
			//debut detection
			btnRecord.setText("Stop");
			SoundManager.getInstance().recordAudio();		
		    }

		    isRecording = !isRecording;
		    btnDetect.setEnabled(!isRecording);

		}});
	}
	return btnRecord;
    }

    /**
     * @return JButton
     */
    public JButton getBtnDetect() {
	if (btnDetect == null) {
	    logger.debug("Construction btnDetect");
	    
	    btnDetect = new JButton("Detect");
	    btnDetect.setBounds(27, 215, 116, 45);

	    btnDetect.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    getLblresultat().setText("");

		    logger.debug("[MainFrame][actionPerformed] : clic sur bouton detect");

		    if (isDetecting) {		
			panelAlert.setBackground(Color.GREEN);
			panelDetection.setBackground(Color.GREEN);
			btnDetect.setText("Detect");
			SoundManager.getInstance().stopDetecting();
			
		    } else {
			//debut detection
			btnDetect.setText("Stop");			
			SoundManager.getInstance().detectSound();		
		    }

		    isDetecting = !isDetecting;
		    btnRecord.setEnabled(!isDetecting);

		}});

	}
	return btnDetect;
    }


    /**
     * @return JButton
     */
    public JButton getBtnPlay() {
	if (btnPlay == null) {
	    logger.debug("Construction btnPlay");
	    
	    btnPlay = new JButton("Play");
	    btnPlay.setBounds(304, 215, 116, 45);

	    btnPlay.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    getLblresultat().setText("");

		    logger.debug("[MainFrame][actionPerformed] : clic sur bouton play");

		    if (isPlaying) {
			SoundManager.getInstance().stopPlaying();
			btnPlay.setText("Play");			
		    } else {
			btnPlay.setText("Stop");
			SoundManager.getInstance().playSound("samples/alarme.wav", 1);	
		    }

		    isPlaying = !isPlaying;

		}});
	}
	return btnPlay;
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {

	logger.info("Debut initialize");
	
	SoundManager.getInstance().addSoundManagerListener(this);

	//frame = new JFrame();
	this.setResizable(false);
	this.setBounds(100, 100, 450, 300);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	getContentPane().setLayout(null);

	getContentPane().add(getLblresultat());

	panelDetection = new JPanel();
	getLblresultat().setLabelFor(panelDetection);
	panelDetection.setBorder(new LineBorder(new Color(0, 102, 0), 3, true));
	panelDetection.setBounds(27, 41, 100, 100);
	panelDetection.setBackground(Color.GREEN);
	panelDetection.setForeground(Color.BLACK);

	getContentPane().add(panelDetection);
	panelDetection.setLayout(null);

	panelAlert = new JPanel();
	panelAlert.setLayout(null);
	panelAlert.setForeground(Color.BLACK);
	panelAlert.setBorder(new LineBorder(new Color(0, 102, 0), 3, true));
	panelAlert.setBackground(Color.GREEN);
	panelAlert.setBounds(151, 41, 100, 100);
	getContentPane().add(panelAlert);
	panelAlert.setLayout(null);

	JLabel lblDetection = new JLabel("Detection");
	lblDetection.setBounds(27, 16, 46, 14);
	getContentPane().add(lblDetection);

	JLabel lblAlert = new JLabel("Alert");
	lblAlert.setBounds(151, 16, 46, 14);

	this.getContentPane().add(lblAlert);

	this.getContentPane().add(getBtnDetect());
	this.getContentPane().add(getBtnRecord());
	this.getContentPane().add(getBtnPlay());

	logger.info("Fin initialize");

    }

    /* (non-Javadoc)
     * @see com.evesoft.msd.listeners.DetectionListener#update(com.evesoft.msd.Events.DetectionEvent)
     */
    @Override
    public void update(SoundManagerEvent evt) {

	logger.info("[MainFrame][update] : evt={}", evt.toString());
	
	switch(evt.getMessage()) {

	case "ALERT_ON" :
	    panelAlert.setBackground(Color.RED);
	    SoundManager.getInstance().playSound("sounds/alert.wav", 4);	    
	    break;

	case "ALERT_OFF" :
	    panelAlert.setBackground(Color.GREEN);
	    SoundManager.getInstance().stopPlaying();
	    break;
	    
	case "DETECTION_ACTIVE" :
	    panelDetection.setBackground(Color.RED);
	    break;
	    
	case "DETECTION_INACTIVE" :
	    panelDetection.setBackground(Color.GREEN);
	    break;
	    
	case "READING_OVER" :
	    getBtnPlay().setText("Play");
	    this.isPlaying = false;
	    break;
	}
    }





}
