package com.evesoft.msd.Events;

/**
 * @author Jérôme
 *
 */
public class SoundManagerEvent {

    private String message;

    /**
     * @return String
     */
    public String getMessage() {
	return message;
    }

    /**
     * @param message
     */
    public SoundManagerEvent(String message) {
	this.message = message;
    }

    @Override
    public String toString() {
	return "DetectionEvent [message=" + message + "]";
    }
    
    
    
}
