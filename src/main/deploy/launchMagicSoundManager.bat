@echo off
setLocal EnableDelayedExpansion
set CLASSPATH="
for /R ./lib %%a in (*.jar) do (
   set CLASSPATH=!CLASSPATH!;%%a
)

set CLASSPATH=!CLASSPATH!"
echo !CLASSPATH!

java -Dlog4j.configuration=log4j2.xml -Xms64M -Xmx256M -cp %CLASSPATH% com.evesoft.msd.launcher.LauncherUI

pause