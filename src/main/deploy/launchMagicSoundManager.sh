#!/bin/bash
#set -x

# =============================================================================
# Application : 
# Description : 
#					
# =============================================================================
# Document confidentiel - diffusion et reproduction interdite
# =============================================================================

# =============================================================================
# Utilisation
# -----------------------------------------------------------------------------
# Syntaxe      : ./launchMagicSoundManager.sh
# Param Entree : VACATION
# Param Sortie : 0 si pas d'erreur, 1 si parametres incorrects, 2 si erreur technique
# =============================================================================
import_jar()
{
	# tant qu'il y a des parametres
	while [ $# -ne 0 ]
	do
		CP=$1:$CP
		# passage aux parametres suivant 
		shift
	done
}

# =============================================
# === Procedure pour valider des parametres ===
# =============================================
Param_Validation()
{
	if [ $NBPARAM -gt 1 ]
	then
		echo "ERREUR  : le nombre de parametres est incorrect."
		exit $RETOUR_PARAM
	fi
} # === Fin Param_Validation() ==

# ================================
# === Procedure Traitement principal   ===
# ================================
Appel_Procedure_Fonction()
{
	if [ $NBPARAM -gt 0 ] 
	then
		java -Dlog4j.configuration=log4j2.xml -Xms64M -Xmx256M -cp $CP com.evesoft.msd.launcher.LauncherUI
	else
		java -Dlog4j.configuration=log4j2.xml -Xms64M -Xmx256M -cp $CP com.evesoft.msd.launcher.LauncherUI
	fi

	#Assigne le ERRORLEVEL dans la variable CODE_RETOUR
	
    CODE_RETOUR=$?
} 
# === Fin Appel_Procedure_Fonction() ===#

# ================================
# === Procedure Principale ===
# ================================
ProcPrincipale()
{
	Param_Validation
	Appel_Procedure_Fonction
}
# === Fin Proc == #
 
	echo "*** DEBUT : $(date) ***"
	
	NBPARAM=$#

	dir=`dirname $0`
	cd $dir

	if [ $NBPARAM -gt 0 ]
	then
		VACATION=$1
	fi
  
	LISTE_JAR=`ls lib/*.jar *.properties`
	import_jar $LISTE_JAR
 
	ProcPrincipale
 
	if [ $CODE_RETOUR = 0 ] 
	then
		echo "Traitement OK"
	else
		echo "Traitement KO. Code retour = $CODE_RETOUR"
	fi
  
	echo "*** FIN : $(date) ***"
	
	exit $CODE_RETOUR
	
# =============================================================================
# Fin du fichier
# =============================================================================
